package com.br.facade.base;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.sql.DataSource;

import com.br.dao.base.DAOManager;

public abstract class FacadeBaseBean {

	@Resource(mappedName="java:jboss/postgresDS") 
	protected DataSource ds;
	
	@Resource 
	protected EJBContext context;
	
	protected DAOManager daoManager = DAOManager.getInstance();
}
