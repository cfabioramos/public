package com.br.facade;

import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.br.dao.UserDAO;
import com.br.dao.base.DAOManager;
import com.br.facade.base.FacadeBaseBean;
import com.br.pojo.Table;

/**
 * Session Bean implementation class UserBean
 */
@TransactionManagement(TransactionManagementType.CONTAINER)
@Stateless(mappedName = "UserBean")
@LocalBean
public class UserBean extends FacadeBaseBean{
	
    /**
     * Default constructor.
     */
    public UserBean() {
        // TODO Auto-generated constructor stub
    }
    
    public Integer findTotalUsers() throws SQLException{
    	
    	DAOManager dao = DAOManager.getInstance();
    	UserDAO fDao = (UserDAO)dao.getDAO(Table.User); 
    	return fDao.count();
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void insert() throws SQLException{
    	
    	try{
    		UserDAO fDao = (UserDAO)daoManager.getDAO(Table.User); 
    		fDao.insert();
    		daoManager.commit();
    	}
    	catch (Exception e){
    		daoManager.rollback();
    		System.out.println("Deu pau!");
    		throw new EJBException ("Transaction failed due to SQLException: " + e.getMessage());
    	}
    }

}
