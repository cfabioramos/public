package com.br.controller;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.br.facade.UserBean;

@RequestScoped
@ManagedBean
public class User {

	@PostConstruct
	public void init() {
		System.out.println(" Bean executado! ");
	}
	
	@EJB
	UserBean userBean;

	public String getMessage() {
		return "Hello World JSF!";
	}
	
	public Integer getId() {
		try {
			
			userBean.insert();
			
			return userBean.findTotalUsers();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
