package com.br.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.br.dao.base.GenericDAO;
import com.br.pojo.User;

public class UserDAO extends GenericDAO<User> {

	// Private
	private final static String TABLENAME = "\"User\"";

	public UserDAO(Connection con) {
		super(con, TABLENAME);
	}

	@Override
	public int count() throws SQLException {
		String query = "SELECT COUNT(*) AS count FROM " + this.tableName;
		PreparedStatement counter;
		try {
			counter = this.con.prepareStatement(query);
			ResultSet res = counter.executeQuery();
			res.next();
			return res.getInt(1);
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public void insert() throws SQLException {
		String query = "INSERT INTO " + this.tableName + " (ID, NAME) VALUES (11, 'Teste 11') ";
		PreparedStatement counter;
		try {
			counter = this.con.prepareStatement(query);
			counter.execute();
		} catch (SQLException e) {
			throw e;
		}
	}
}