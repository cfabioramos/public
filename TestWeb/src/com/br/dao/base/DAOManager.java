package com.br.dao.base;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.br.dao.UserDAO;
import com.br.pojo.Table;

/**
 * 
 * @author F�bio
 * 
 * You can also implement methods that encapsulate setAutoCommit(), commit() and rollback() 
 * from the Connection so you can have a better handling of your transactions. 
 * What I also did is, instead of just holding a Connection, DAOManager also holds a 
 * PreparedStatement and a ResultSet. So, when calling close() it also closes both. 
 * A fast way of closing statements and result sets!
 */

public class DAOManager {

    public static DAOManager getInstance() {
        return DAOManagerSingleton.INSTANCE.get();
    }
    
    private static class DAOManagerSingleton {

        public static final ThreadLocal<DAOManager> INSTANCE;
        static
        {
            ThreadLocal<DAOManager> dm;
            try
            {
                dm = new ThreadLocal<DAOManager>(){
                    @Override
                    protected DAOManager initialValue() {
                        try
                        {
                            return new DAOManager();
                        }
                        catch(Exception e)
                        {
                            return null;
                        }
                    }
                };
            }
            catch(Exception e){
                dm = null;
            }
            INSTANCE = dm;
        }        
    }
    
    //Private
    private DataSource src;
    private Connection con;

    private DAOManager() throws Exception {
        try
        {
            InitialContext ctx = new InitialContext();
            this.src = (DataSource)ctx.lookup("java:jboss/postgresDS");
        }
        catch(Exception e) { throw e; }
    }

    public GenericDAO getDAO(Table t) throws SQLException 
    {
        try
        {
            if(this.con == null || this.con.isClosed())   
                this.open();
        }
        catch(SQLException e){ 
        	throw e; 
        }
        
        switch(t)
        {
        case User:
            return new UserDAO(this.con);
        default:
            throw new SQLException("Trying to link to an unexistant table.");
        }
    }

    public void open() throws SQLException {
        try
        {
            if(this.con==null || this.con.isClosed())
                this.con = src.getConnection();
            this.con.setAutoCommit(false);
        }
        catch(SQLException e) { throw e; }
    }

    public void close() throws SQLException {
        try
        {
            if(this.con!=null && !this.con.isClosed())
                this.con.close();
        }
        catch(SQLException e) { throw e; }
    }
    
    public void commit() throws SQLException{
    	this.con.commit();
    }
    
    public void rollback() throws SQLException{
    	this.con.rollback();
    }
    
    @Override
    protected void finalize() throws Throwable
    {

        try{ this.close(); }
        finally{ super.finalize(); }

    }
}